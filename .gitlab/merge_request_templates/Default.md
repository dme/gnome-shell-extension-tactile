### Description

<!-- Describe the changes -->

### Rationale

<!-- Describe why the changes are needed -->

### Checklist

- [ ] I have followed the guidelines at https://gjs.guide/extensions/
- [ ] I have tested a clean installation of the extension (preferably in a fresh virtual machine)
- [ ] I have not changed the version in `metadata.json` (will be incremented when releasing)
